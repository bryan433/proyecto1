#include <stdio.h>
#include <stdlib.h>
#include <time.h> 
#define fila 8
#define	columna 8

int sorteo_quien_parte_pvp(char nickname1[10],char nickname2[10]){
	int quien_parte_pvp;
	 quien_parte_pvp=(rand()%2)+1;
	 if (quien_parte_pvp==1){
		printf ("La partida la inicia %s\n",nickname1);
		printf ("\n");
	 }
	else if (quien_parte_pvp==2){
		printf ("La partida la inicia %s\n",nickname2);
		printf ("\n");
	 } 
	return quien_parte_pvp; 
}
int sorteo_quien_parte_pve(char nickname1[10]){
	int quien_parte_pve;
	 quien_parte_pve=(rand()%2)+1;
	 if (quien_parte_pve==1){
		printf ("La partida la inicia %s\n",nickname1);
		printf ("\n");
	 }
	else if (quien_parte_pve==2){
		printf ("La partida la inicia el computador\n");
		printf ("\n");
	 } 
	return quien_parte_pve;
}

void sorteo_fichas_pvp(char nickname1[10],char nickname2[10]){
	int ficha_1=79;
	int ficha_2=88;
		printf ("\n");
		printf ("\n%s le corresponde el simbolo %c\n",nickname1,ficha_2);
		printf ("%s le corresponde el simbolo %c\n",nickname2,ficha_1); 
}

void sorteo_fichas_pve(char nickname1[10]){
	int ficha_1=79;// letra O
	int ficha_2=88;// letra x
			printf ("\n");
			printf ("\n%s le corresponde el simbolo %c\n",nickname1,ficha_2);
			printf ("\nla computadora le corresponde el simbolo %c\n",ficha_1); 
}

void instrucciones_de_juego(){
	char  opcion;
	printf ("¿Sabe jugar 4 en fila?: si <s> || no <n>\n");
		scanf ("%c",&opcion);
	if (opcion=='n')
	{
		printf ("1.La modalidad de juego <clasica> será jugador1 v/s jugador2 (PvP)\n");
		printf ("2.Si desea puede optar a la modalidad <solitario> será nickname 1 v/s computador (PvE)\n");
		printf ("¿Como jugar?:\nEl juego consiste en colocar cuatro fichas en una fila continua vertical, horizontal o diagonal.\n");
		printf ("Empecemos a jugar!\n");
	}
	else if(opcion=='s')
	{
		printf ("Empecemos a jugar!\n");
		
	}
		
}
void llenar_tablero(int tablero[fila][columna]){
		for (int i=0;i<fila;i++){
			for (int j=0;j<columna;j++){
				tablero[i][j]=32;
			}
		}
}
void imprimir_tablero(int tablero[fila][columna]){
	printf ("\n");
	for(int j=0;j<columna+1;j++) 
		printf (" ");
		printf("\n");

		for(int i=0;i<fila;i++) {
			printf(" ");
     
    printf("    |");
   
    for(int j=0;j<columna;j++) {
      printf(" ");
      printf("%c",tablero[i][j]);
      printf(" |");
    }
		printf(" ");
    if (i==0) 
		printf("\n ");
    else if (i==1) 
		printf("\n ");
    else 
		printf("\n  ");
		printf(" -");
    for(int j=0;j<columna+1;j++) 
		printf("----");
		printf("\n");
  }
	printf("       ");
  for(int j=0;j<columna;j++) 
	printf("%d   ",j);
}
int revisar_empate(int tablero[fila][columna]){
	int i;
	int j;
	int empate=0;
	for(i=0; i<8; i++){
		for(j=0; j<8; j++){
			if(tablero[i][j]!=32){
				empate++;
			}
		}
	}
	return empate;
}
	
int revision_tablero(int tablero[8][8], int columna_seleccionada){
	int i=7;
	while (tablero[i][columna_seleccionada]==88 || tablero[i][columna_seleccionada]==79 ){
		i=i-1;
		
	}
	return i;
	
}
int revisar_ganador1(int tablero[fila][columna], int columna_seleccionada, int fila_de_mas_arriba, char nickname1[10], int ganar){
			
			//revisar en vertical
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba+1][columna_seleccionada]==88 && tablero[fila_de_mas_arriba+2][columna_seleccionada]==88 && tablero[fila_de_mas_arriba+3][columna_seleccionada]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			//revisar en diagonal
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba-1][columna_seleccionada+1]==88 && tablero[fila_de_mas_arriba-2][columna_seleccionada+2]==88 && tablero[fila_de_mas_arriba-3][columna_seleccionada+3]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba+1][columna_seleccionada-1]==88 && tablero[fila_de_mas_arriba+2][columna_seleccionada-2]==88 && tablero[fila_de_mas_arriba+3][columna_seleccionada-3]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}			
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba-1][columna_seleccionada-1]==88 && tablero[fila_de_mas_arriba-2][columna_seleccionada-2]==88 && tablero[fila_de_mas_arriba-3][columna_seleccionada-3]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba+1][columna_seleccionada+1]==88 && tablero[fila_de_mas_arriba+2][columna_seleccionada+2]==88 && tablero[fila_de_mas_arriba+3][columna_seleccionada+3]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba+1][columna_seleccionada-1]==88 && tablero[fila_de_mas_arriba-1][columna_seleccionada+1]==88 && tablero[fila_de_mas_arriba-2][columna_seleccionada+2]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba-1][columna_seleccionada+1]==88 && tablero[fila_de_mas_arriba+1][columna_seleccionada-1]==88 && tablero[fila_de_mas_arriba+2][columna_seleccionada-2]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba+1][columna_seleccionada+1]==88 && tablero[fila_de_mas_arriba-1][columna_seleccionada-1]==88 && tablero[fila_de_mas_arriba-2][columna_seleccionada-2]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba-1][columna_seleccionada-1]==88 && tablero[fila_de_mas_arriba+1][columna_seleccionada+1]==88 && tablero[fila_de_mas_arriba+2][columna_seleccionada+2]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			//revisar en horisontal 
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba][columna_seleccionada-1]==88 && tablero[fila_de_mas_arriba][columna_seleccionada-2]==88 && tablero[fila_de_mas_arriba][columna_seleccionada-3]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba][columna_seleccionada+1]==88 && tablero[fila_de_mas_arriba][columna_seleccionada+2]==88 && tablero[fila_de_mas_arriba][columna_seleccionada+3]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba][columna_seleccionada+1]==88 && tablero[fila_de_mas_arriba][columna_seleccionada-1]==88 && tablero[fila_de_mas_arriba][columna_seleccionada-2]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==88 && tablero[fila_de_mas_arriba][columna_seleccionada-1]==88 && tablero[fila_de_mas_arriba][columna_seleccionada+1]==88 && tablero[fila_de_mas_arriba][columna_seleccionada+2]==88){
				printf ("\ngana %s el juego 4 rayas", nickname1);
				ganar=ganar+1;
			}
			return ganar;
		}
int revisar_ganador2 (int tablero[fila][columna], int columna_seleccionada, int fila_de_mas_arriba, char nickname2[10], int ganar){
			
			//revisar en vertical
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+3][columna_seleccionada]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			//revisar en diagonal
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba-2][columna_seleccionada+2]==79 && tablero[fila_de_mas_arriba-3][columna_seleccionada+3]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba-2][columna_seleccionada+2]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada-2]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba-2][columna_seleccionada-2]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada+2]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada-2]==79 && tablero[fila_de_mas_arriba+3][columna_seleccionada-3]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba-2][columna_seleccionada-2]==79 && tablero[fila_de_mas_arriba-3][columna_seleccionada-3]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada+2]==79 && tablero[fila_de_mas_arriba+3][columna_seleccionada+3]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			//revisar en horisontal
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+2]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+3]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-2]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-3]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-2]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+2]==79){
				printf ("\ngana %s el juego 4 rayas", nickname2);
				ganar=ganar+1;
			}
			return ganar;
		}
		
int revisar_ganador_computador(int tablero[fila][columna], int columna_seleccionada, int fila_de_mas_arriba,int ganar){
			
			//revisar en vertical
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+3][columna_seleccionada]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			//revisar en diagonal	
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba-2][columna_seleccionada+2]==79 && tablero[fila_de_mas_arriba-3][columna_seleccionada+3]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba-2][columna_seleccionada+2]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada-2]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba-2][columna_seleccionada-2]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada+2]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada-2]==79 && tablero[fila_de_mas_arriba+3][columna_seleccionada-3]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba-1][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba-2][columna_seleccionada-2]==79 && tablero[fila_de_mas_arriba-3][columna_seleccionada-3]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba+1][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba+2][columna_seleccionada+2]==79 && tablero[fila_de_mas_arriba+3][columna_seleccionada+3]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			//revisar en horisontal
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+2]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+3]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-2]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-3]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-2]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			if (tablero[fila_de_mas_arriba][columna_seleccionada]==79 && tablero[fila_de_mas_arriba][columna_seleccionada-1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+1]==79 && tablero[fila_de_mas_arriba][columna_seleccionada+2]==79){
				printf ("\ngana el computador el juego 4 rayas");
				ganar=ganar+1;
			}
			return ganar;
		}
		
void ingresar_ficha_pvp(int tablero[fila][columna], char nickname1[10], char nickname2[10]){
	int jugadas_totales;
	int columna_seleccionada;
	int fila_de_mas_arriba;
	int orden_partida;
	int ganar=0;
	int empate;
	sorteo_fichas_pvp(nickname1,nickname2);
	orden_partida=sorteo_quien_parte_pvp(nickname1,nickname2);	
		while(jugadas_totales<=64){
			if (orden_partida==1){
				printf ("\n %s en que columna desea colocar la ficha: ",nickname1);
				scanf ("%d", &columna_seleccionada);
				if (columna_seleccionada>=0 || columna_seleccionada<8){	
					fila_de_mas_arriba=revision_tablero(tablero,columna_seleccionada);
					tablero[fila_de_mas_arriba][columna_seleccionada]=88;
					imprimir_tablero(tablero);
					ganar=revisar_ganador1(tablero,columna_seleccionada,fila_de_mas_arriba,nickname1,ganar);
					if (ganar>0){
						printf ("GANO el jugador %s", nickname1);
						break;
					}
					else{
						empate=revisar_empate(tablero);
						if (empate==64){
							printf ("\n esto termina en empate");
							break;
						}
						else{
							orden_partida=2;
						}
					}
				}
			}
			if (orden_partida==2){
				printf ("\n %s en que columna desea colocar la ficha: ",nickname2);
				scanf ("%d", &columna_seleccionada);
				if (columna_seleccionada>=0 || columna_seleccionada<8){	
					fila_de_mas_arriba=revision_tablero(tablero,columna_seleccionada);
					tablero[fila_de_mas_arriba][columna_seleccionada]=79;
					imprimir_tablero(tablero);
					jugadas_totales++;
					ganar=revisar_ganador2 (tablero,columna_seleccionada,fila_de_mas_arriba,nickname2,ganar);
					if (ganar>0){
						printf ("GANO el jugador %s", nickname2);
						break;
					}
					else{
						empate=revisar_empate(tablero);
						if (empate==64){
							printf ("\n esto termina en empate");
							break;
						}
						else{
							orden_partida=1;
						}
					}
				}
			}
		}
	}
	
void ingresar_ficha_pve(int tablero[fila][columna], char nickname1[10]){
	int jugadas_totales;
	int columna_seleccionada;
	int fila_de_mas_arriba;
	int orden_partida;
	int ganar;
	int empate;
	sorteo_fichas_pve(nickname1);
	orden_partida=sorteo_quien_parte_pve(nickname1);	
		while(jugadas_totales<=64){
			if (orden_partida==1){
				printf ("\n%s en que columna desea colocar la ficha: ",nickname1);
				scanf ("%d", &columna_seleccionada);
				if (columna_seleccionada>0 || columna_seleccionada<8){	
					fila_de_mas_arriba=revision_tablero(tablero,columna_seleccionada);
					tablero[fila_de_mas_arriba][columna_seleccionada]=88;
					imprimir_tablero(tablero);	
					ganar=revisar_ganador1(tablero,columna_seleccionada,fila_de_mas_arriba,nickname1,ganar);
					if (ganar>0){
						printf ("GANO el jugador %s", nickname1);
						break;
					}
					else{
						empate=revisar_empate(tablero);
						if (empate==64){
							printf ("\n esto termina en empate");
							break;
						}
						else{
							orden_partida=2;
						}
					}
				}
			}
			if (orden_partida==2){
				columna_seleccionada=(rand()%8)+0;
				printf ("\nel computador jugo en la columna %d", columna_seleccionada);
				if (columna_seleccionada>0 || columna_seleccionada<8){	
					fila_de_mas_arriba=revision_tablero(tablero,columna_seleccionada);
					tablero[fila_de_mas_arriba][columna_seleccionada]=79;
					imprimir_tablero(tablero);
					jugadas_totales++;
					ganar=revisar_ganador_computador(tablero,columna_seleccionada,fila_de_mas_arriba,ganar);
					if (ganar>0){
						printf ("GANO el computador");
						break;
					}
					else{
						empate=revisar_empate(tablero);
						if (empate==64){
							printf ("\n esto termina en empate");
							break;
						}
						else{
							orden_partida=1;
						}
					}
				}
			}
		}
	}


int modo_de_juego(int tablero[fila][columna]){
	int modo_juego;
	char nickname1[10]; 
	char nickname2[10];
		printf ("¿Qué modalidad de juego elijira PvP <1> || PvE <2>\n");
		scanf ("%d",&modo_juego);
	if (modo_juego==1){
		printf ("Seleccionó PvP\nIngrese nickname 1:\n");
		scanf ("%s",nickname1); 
		printf ("Ingrese nickname 2:\n");
		scanf ("%s",nickname2);
		llenar_tablero(tablero);
		imprimir_tablero(tablero);
		ingresar_ficha_pvp(tablero,nickname1,nickname2);
	}
	if (modo_juego==2){
		printf ("Seleccionó PvE:\nEscriba su nickname:\n");
		scanf ("%s",nickname1);
		llenar_tablero(tablero);
		imprimir_tablero(tablero);
		ingresar_ficha_pve(tablero,nickname1);
	}

	return modo_juego;
}

int main(){	
	char jugar;
	srand(time(NULL));
	int tablero[fila][columna];
	printf ("_________________INICIO_________________\n");
	instrucciones_de_juego();
	modo_de_juego(tablero);
	while (jugar!='n'){
		printf ("\n¿desea volver a jugar: si <s> || no <n> ?: ");
		scanf (" %c",&jugar);
		if (jugar=='n'){
				system("clear");
				printf ("¡GRACIAS POR JUGAR!");
		}
		if (jugar=='s'){
			modo_de_juego(tablero);			
		}
	}
	return 0;
}
